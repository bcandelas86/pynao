"""
Run DFT+TDDFT calculation on one go.
DFT calulation is first done using Siesta via the ASE wrapper.
Then TDDFT calculations are performed with pynao
"""
import os
import numpy as np

from pynao import tddft_tem
from ase.units import Ry, eV, Ha

dname = os.getcwd() 

eps = 0.15/Ha
freq = np.arange(0.0, 10.0, 0.05)/Ha
# Then run TDDFT calculation with pynao
# Calculate the kernel
td = tddft_tem(label='siesta', cd=dname, verbosity=4, freq=freq,
               iter_broadening=eps, tol_loc=1e-4, tol_biloc=1e-6, jcutoff=7,
               level=0, xc_code='LDA,PZ',
               krylov_options={"tol": 1.0e-5, "atol": 1.0e-8})

# Calculate EELS
#z_shifts = np.array([40.0, 30.0, 24.0, 20.0, 10.0, 5.0, 0.01])
z_shifts = np.array([0.01])

for z in z_shifts:
    pnonin = -td.get_spectrum_nonin(beam_offset=np.array([0.0, 0.0, z]),
                                    velec=np.array([0.0, 75.0, 0.0]))
    np.save("EELS_nonin_zshift_{0:.2f}Bohr.npy".format(z), pnonin)
    np.save("density_change_prod_basis_nonin_tem_zshift_{0:.2f}Bohr.npy".format(z), td.dn0)

    p_iter = -td.get_spectrum_inter(beam_offset=np.array([0.0, 0.0, z]),
                                    velec=np.array([75.0, 0.0, 0.0]))

    np.save("EELS_inter_zshift_{0:.2f}Bohr.npy".format(z), p_iter)
    np.save("density_change_prod_basis_inter_tem_zshift_{0:.2f}Bohr.npy".format(z), td.dn)
