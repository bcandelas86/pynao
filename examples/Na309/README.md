# Sodium 309 cluster

This example shows how the polarizability and the electronic density change
distribution for a Na309 cluster can be easily obtained using TDDFT.
Many simulations shown in Ref. [1, 2] were obtained using similar methods.

An anlysis of the iterative solver tolerance impact on the polarizability is as
well presented in this example.

## Iterative solver analysis

In order to solve the linear system of equations at the center of the `tddft_iter`
method, PyNAO make use of the iterative solver available in the
[SciPy package](https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html#module-scipy.sparse.linalg)
lgmres has shown to be the more reliable method, but bicgstab is often slightly
faster. In this section we analyze the impact of the lgmres iterative solver on
the performance and accuracy. Results are shown in figure `Na309_tolerance_analysis.svg`.

The polarizability is shown on the left panel, while on the right panel the
total number of iterations (blue dots, left axis) and the relative error
(red squares, right axis) are shown.

We can see that a tolerance of 1.0e-4 is the most optimal, accurate enough
without taking too many iterations.

In this test, noth `tol` and `atol` parameter were changed in the same time.

## List of files

* `siesta.fdf`: siesta input file
* `geometry.siesta.fdf`: siesta geometry file
* `Na.psf`: psudopotential for siesta calculations
* `Na309.xyz`: system geometry in xyz format
* `runtddft.py`: simple script to perform TDDFT calculations with PyNAO
* `runtddft_tem.py`: perform TDDFT for TEM simulations.
* `get_spatial_dist.py`: calculate the density change distribution in space.
Must be run after `runtddft.py` or `runtddft_tem.py`.
* `runtddft_tolerance_analysis.py`: launch `tddft_iter` for several iterative solver
tolerances. Used to generate the graph `Na309_tolerance_analysis.svg`.
* `get_spatial_dist_tolerance.py`: generate the density change distribution
for the different tolerances.
* `Na309_tolerance_analysis.svg`: iterative solver tolerance analysis.


## References

1. PySCF-NAO: An efficient and flexible implementation of linear response
time-dependent density functional theory with numerical atomic orbitals,
P. Koval, M. Barbry and D. Sanchez-Portal, Computer Physics Communications
2. Plasmons in Nanoparticles: Atomistic Ab Initio Theory for Large Systems, M. Barbry,
(https://cfm.ehu.es/cfm_news/phd-thesis-defense-marc-barbry/)
