"""
Run DFT+TDDFT calculation on one go.
DFT calulation is first done using Siesta via the ASE wrapper.
Then TDDFT calculations are performed with pynao
"""
import os
import numpy as np

import ase.io as io
from ase.calculators.siesta import Siesta
from ase.units import Ry, eV, Ha
from pynao import tddft_iter

# First run DFT calculation with Siesta

dname = os.getcwd() 
fname = "CH4.xyz"

# load the molecule geometry
atoms = io.read(fname)

# set-up the Siesta parameters
siesta = Siesta(
      mesh_cutoff=250 * Ry,
      basis_set='DZP',
      pseudo_qualifier='gga',
      xc="PBE",
      energy_shift=(25 * 10**-3) * eV,
      fdf_arguments={
        'SCFMustConverge': False,
        'COOP.Write': True,
        'WriteDenchar': True,
        'PAO.BasisType': 'split',
        'DM.Tolerance': 1e-4,
        'DM.MixingWeight': 0.01,
        "MD.NumCGsteps": 0,
        "MD.MaxForceTol": (0.02, "eV/Ang"),
        'MaxSCFIterations': 10000,
        'DM.NumberPulay': 4,
        'XML.Write': True,
        "WriteCoorXmol": True})

atoms.set_calculator(siesta)

# Run Siesta calculation
e = atoms.get_potential_energy()
print("DFT potential energy", e)


# Then run TDDFT calculation with pynao
# Calculate the kernel
td = tddft_iter(label='siesta', cd=dname, verbosity=2, krylov_solver="lgmres",
                krylov_options={"tol": 1.0e-3, "atol": 1.0e-3, "outer_k": 5})

# Calculate polarizability
freq = np.arange(0.0, 5.0, 0.05)/Ha + 1j * td.eps
p_mat = td.comp_polariz_inter_Edir(freq, Eext=np.array([1.0, 1.0, 1.0]),
                                   tmp_fname="pol.tmp")
np.save("Pmat.npy", p_mat)
