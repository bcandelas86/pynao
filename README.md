# PyNAO

PyNAO the Python Numerical Atomic Orbitals.

PyNAO is designed to perform efficient many-body perturbation theory methods 
with numerical atomic orbitals. Actual implemented methods are

* TDDFT (light and electronic perturbation, i.e., EELS)
* G0W0 (GW is ready ??)
* BSE

The code needs inputs from previous DFT calculations. Supported DFT codes are

* [Siesta](https://departments.icmab.es/leem/siesta/)
* [PySCF](https://sunqm.github.io/pyscf/)

For more details, refer to the project [webpage](https://mbarbrywebsite.ddns.net/pynao/doc/html).

## Installation

    apt-get update
    apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
    pip3 install -r requirements.txt
    export CC=gcc && export FC=gfortran && export CXX=g++
    python setup.py bdist_wheel
    pip install dist/pynao-0.1.1-py3-none-any.whl

Furtrher details can be found on the installation instruction
[page](https://mbarbrywebsite.ddns.net/pynao/doc/html/install.html).

## Docker

To run PyNAO within a Jupyter-lab environment use the following command

    docker run -p 8888:8888 -it mbarbry/pynao:latest

You can connect to the notebook with your web
browser via the address http://127.0.0.1:8888/lab

The PyNAO tutorials are presents in the `tutorial-pynao` folder.

You can also just run a Python script in your current folder via the command

    docker run -v $(pwd):/usr/src/app -it mbarbry/pynao:latest /usr/bin/python3 test.py

## Binder

You can try out PyNAO on mybinder.org

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/mbarbry%2Fpynao/master?filepath=examples%2Ftutorials%2F)


## Bug report

* Barbry Marc <marc.barbry@mailoo.org>
* Koval Peter <koval.peter@gmail.com>
