# PyNAO documentation

The PyNAO documentation is build using [sphinx](https://www.sphinx-doc.org/en/master/).
It uses the theme `sphinx_rtd_theme` that must be installed manually

    pip install sphinx_rtd_theme

The documentation can be simply build by

    make html

The code documentation can be found in `source/pynao.rst`. New files can be
added manually or added by running (pynao must be installed)

    sphinx-apidoc -o source/ ../pynao
