.. _DFT:

=======================
Runing DFT calculations
=======================

Before to be able to calculate the excited states properties of a
system with PyNAO, the ground state properties must be calculated.

Currently, PyNAO supports DFT inputs from

- `Siesta <https://departments.icmab.es/leem/siesta/>`__
- `PySCF <http://pyscf.org/>`_


Runing PySCF calculations
-------------------------

The PySCF DFT code ( [Qiming2018-pyscf]_, [Qiming2020-pyscf]_) is a recent code
that developped pretty fast in the last years. The PyNAO code was originaly part
of the PySCF code, [koval2018pynao]_, and it kept strong links with
PySCF and uses several of its functionnalities. Therefore, PySCF must be installed
in order to use PyNAO. PySCF is thus the most straighforward way to get DFT inputs
for PyNAO. PySCF can be simply run with the following lines

.. code::

    from pyscf import gto, scf

    mol = gto.M(atom=yourMolecule, basis='yourBasis')
    gto_mf = scf.UHF(mol)
    gto_mf.kernel()

Please refer to `PySCF documentation <http://pyscf.org/documentation.html>`_ for
more details on how to use PySCF. Then, PyNAO can be easily launch via

.. code::

    from pynao import tddft_iter

    td = tddft_iter(mf=gto_mf, gto=mol)

Here we used the example of TDDFT, but GW and BSE methods are launch in a similar
fashion.

The :ref:`tutorial 1 <tutorial1-tddftCH4-pyscf>` gives an
introduction to TDDFT PyNAO calculations with PySCF for the DFT part.

Runing Siesta calculations
--------------------------

The Siesta code ( [siesta2002]_, [siesta2020]_) is a well established DFT code.
In order for PyNAO to use Siesta DFT calculations as inputs, the following options
must be activated in the Siesta ``fdf`` input file

- the option ``COOP.Write`` must be set to ``True``
- the option ``XML.Write`` must be set to ``True``
- take note of the ``SystemLabel`` variable

Note that the option ``COOP.write`` in siesta input exports the Hamiltonian and the
KS eigenstates to the files with *HSX* and *WFSX* extensions, respectively.
The latter store files are essential to start post-processing calculations.

The PyNAO can load Siesta data by passing the keyword ``label``. For example,

.. code::

    from pynao import tddft_iter

    td = tddft_iter(label=SiestaSystemLabel)

where *SiestaSystemLabel* is the value given to ``SystemLabel`` in the siesta
``fdf`` file.

The :ref:`tutorial 2 <tutorial2-tddftCH4-siesta>` gives an
introduction to TDDFT PyNAO calculations with Siesta for the DFT part.
