.. _optimizations:

=============
Optimizations
=============

In order to run PyNAO efficiently, a few parameters must be properly set.
A few things need 

Iterative solver threshold
--------------------------

One of the most important part of PyNAO, is the solution of the system of
linear equations described in equation (2.83) of Ref. [Barbry2018thesis]_.
In order to solve these equation, PyNAO uses the iterative solvers of the
`SciPy library <https://docs.scipy.org/doc/scipy/reference/sparse.linalg.html>`_.

Several solvers are available in the SciPy library. The parameter `krylov_solver`
is used to chose the solver. The `LGMRES <https://docs.scipy.org/doc/scipy/reference/generated/scipy.sparse.linalg.lgmres.html#scipy.sparse.linalg.lgmres>`_ 
solver is recommended. These solvers take a few options that will strongly
influence the accuracy of the solutions and the number of iteration required
in order to achieve convergence. If badly chosen, the solver may not be able
to find a solution, this will make the code very slow and even worse, the solution
found at the end will probably be totally wrong. These options can be passed
via the parameter `krylov_options` which is a dictionnary. The two mains
options are

* `tol`: the relative tolerance for convergence
* `atol`: the absolute tolerance for convergence

The tolerance defines when the residue norm is low enough to satisfy convergence.
If set to high (:math:`> 10^{-3}`) convergence will be fast but the solution will
not be accurate. If set too low, (:math:`< 10^{-6}`), the solution will be 
accurate, but convergence will require much more iterations.

On the left panel on the figure below, we show the polarizability for a sodium
cluster of 309 atoms for several tolerances (using LGMRES solver). On the left
panel the total number of iterations (left axis) and the maximum relative error
(right axis) are shown as function of the tolerance. The relative error is defined
by 

.. math::

    E_{rel} = \frac{|P_{tol} - P_{ref}|}{|P_{ref}|}

where :math:`P_{ref}` is the reference polarizability (here calculated with a
tolerance of :math:`10^{-10}`).

As it can be seen on this figure, a tolerance of :math:`10^{-5}` gives the best
ratio between performances and accuracy for this system.


.. image:: images/Na309_tolerance_analysis.svg
   :scale: 50 %
   :alt: Image not found

.. note::

    For TDDFT calculation, since the calculations of the polarizability is the
    most expensive part, it can be worth to do two calls to
    :mod:`comp_polariz_inter_Edir <pynao.tddft_iter>`.
    One with poor accuracy to get a global feeling of the polarizability shape.
    A second one more accurate around the region of interest. See the
    :ref:`Ag cluster example <tddftAgs309>` for a demonstration.


Kernel sparsity
---------------
