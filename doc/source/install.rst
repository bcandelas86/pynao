.. _install:

============
Installation
============

Requirements
============

In order to install PyNAO, the following packages must be installed

* git
* gcc
* gfortran
* build-essential
* liblapack-dev
* libfftw3-dev
* make
* cmake
* zlib1g-dev
* python3
* python3-pip

PyNAO will need also the following python libraries

* wheel (for installation)
* pytest (for testing)
* pytest-cov (for testing)
* numpy
* scipy<1.5 (for PySCF compatibility)
* h5py
* numba
* pyscf

Some of the examples make use of the `ASE <https://wiki.fysik.dtu.dk/ase/>`_ package,
therefore its installation is recommended. ASE can be handy to launch Siesta
calculations from Python script and to setup the molecular system.

Installation
============

Standard installation
---------------------

On Ubuntu and similar OS, PyNAO can easily be instaaled using the Blas and Lapack
libraries.

>>> apt-get update
>>> apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
>>> pip3 install -r requirements.txt
>>> export CC=gcc && export FC=gfortran && export CXX=g++
>>> python setup.py bdist_wheel
>>> pip install dist/pynao-0.1.1-py3-none-any.whl

.. note::

    By default, the installation will use the ``lib/cmake_user_inc_examples/cmake.user.inc-gnu-openmp``
    ``cmake.arch.inc`` file. For optimal performances, you may want to edit yourself
    the ``cmake.arch.inc`` file. See section **Architecture file cmake.arch.inc**
    for more details.

Installation with Anaconda
--------------------------

For optimal performances, we advise you to use `Anaconda Python <https://www.anaconda.com/>`_
environment. This for three reasons

* First, in Anaconda, NumPy and SciPy libraries are compiled with MKL Blas library (offering in general better performances).
* You can easily use the MKL library included in Anaconda to compile PyNAO (even if it is still possible to use your custom MKL installation).
* At the moment, installing PySCF via pip gives a conflict with the ``libgomp`` library, forbidding PyNAO to be compiled with openmp support. Installing PySCF via conda solves this issues (see PySCF `issue 603 <https://github.com/pyscf/pyscf/issues/603>`_)

In the following, we assume Anaconda is installed under ``${HOME}/anaconda3``.
To properly compile PyNAO with Anaconda, perform the following steps


>>> apt-get update
>>> apt-get install git gcc gfortran build-essential liblapack-dev libfftw3-dev make cmake zlib1g-dev python3 python3-pip
>>> mkdir -p ${HOME}/anaconda3/lib/mkl/lib
>>> ln -s ${HOME}/anaconda3/lib/libmkl* ${HOME}/anaconda3/lib/mkl/lib/
>>> export CC=gcc && export FC=gfortran && export CXX=g++
>>> pip install ase
>>> conda install -c pyscf pyscf

Now, go to ``pynao`` root folder

>>> cp lib/cmake_user_inc_examples/cmake.user.inc-singularity.anaconda.gnu.mkl lib/cmake.arch.inc

Edit the file ``lib/cmake.arch.inc`` to indicate the proper path to MKL library.
You should replace the line ``set(MKLROOT "/opt/conda/lib/mkl/lib")`` by
``set(MKLROOT "${HOME}/anaconda3/lib/mkl/lib")``

then you can finish the installation

>>> python setup.py bdist_wheel
>>> pip install dist/pynao-0.1.1-py3-none-any.whl

Architecture file cmake.arch.inc
--------------------------------

For optimal performances, it is advised to edit the ``cmake.arch.inc`` file.
This file contains platform specific instruction such as BLAS and LAPACK libraries
definition (with MKL for example).

You can find examples in the folder ``lib/cmake_user_inc_examples``


Docker container
----------------

If `Docker <https://www.docker.com/get-started>`_ is installed on your system,
you can easily use PyNAO by simply

    docker run -p 8888:8888 -it mbarbry/pynao:latest

The container starts an jupyter notebook. You can connect to it with your web
browser via the address http://127.0.0.1:8888/lab

The PyNAO tutorials are presents in the `tutorial-pynao` folder.

You can as well simply run a Python script via

    docker run -p 8888:8888 -v $(pwd):/usr/src/app -it mbarbry/pynao:latest /usr/bin/python3 yourScript.py

The docker image contains all the require dependency as well as a serial compilation
of Siesta. More optimal Docker containers of Siesta can be found in the
`Atomic-scale Simulation Environments <https://gitlab.com/espeem/public/atomic-scale-simulation-environments>`_.

Singularity container
---------------------

We provide in ``share/singularity-img`` a collection of
`Singularity <https://sylabs.io/docs/>`_ recipes that are optimized.
If possible, we advise you to use these recipes to use PyNAO. To build the container

>>> sudo singularity build path2pynao/share/singularity-img/pynao-sing.mkl.gpu pynao-image.img

and to run PyNAO script

>>> singularity run pynao-image.img pynao_script.py


GPU support
-----------

Some part of the code can be accelerated using Nvidia GPUs via the cuBlas library.
For this you need to install the `cupy <https://cupy.dev/>`_ library

>>> pip install cupy

or via `conda`

>>> conda install cupy

You can then pass the argument ``GPU=True`` in order to use GPU for your calculations.
GPU will mainly impact performances for large systems.
