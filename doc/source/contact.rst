.. _contact:

=======
Contact
=======

.. _mail:

Mail
====

Do not hesitate to contact the authors for any questions

* Marc Barbry (marc.barbry@mailoo.org)
* Peter Koval (koval.peter@gmail.com)
* Masoud Mansoury (ma.mansoury@gmail.com)

GitLab
======

Feel free to create Merge Requests and Issues on our GitLab page:
https://gitlab.com/mbarbry/pynao
