.. PyNAO documentation master file, created by
   sphinx-quickstart on Sat Aug 15 10:33:46 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

================================
Welcome to PyNAO's documentation
================================

PyNAO the Python Numerical Atomic Orbitals.

PyNAO is designed to perform efficient many-body perturbation theory methods
with numerical atomic orbitals. PyNAO aims to be easy to use.
Actual implemented methods are

* :ref:`TDDFT <TDDFT>` (light and electronic perturbation, i.e., EELS)
* :ref:`G0W0 <gwa>`
* :ref:`BSE <bse>`

PyNAO must load ground state properties from previous DFT calculations.
Supported DFT codes are

* `PySCF <https://sunqm.github.io/pyscf/>`_
* `Siesta <https://departments.icmab.es/leem/siesta/>`_

See the :ref:`DFT <DFT>` page for more details on required inputs.
The :ref:`installation <install>` page gives the instructions for intalling
the program, while the :ref:`example <examples>` page is probably the best to
get easily started once the package is installed.

Try out PyNAO on myBinder
=========================

.. image:: https://mybinder.org/badge_logo.svg
 :target: https://mybinder.org/v2/gl/mbarbry%2Fpynao/master?filepath=examples%2Ftutorials%2F

Known issues
============

See list of issues in the `Gitlab repository <https://gitlab.com/mbarbry/pynao/-/issues>`_

History
=======

PyNAO was originally developed as a branch of the PySCF package under the name
`PySCF-NAO <https://github.com/cfm-mpc/pyscf/tree/nao2>`_, and before this,
it was a Fortran code called `MBPT_LCAO <http://mbpt-domiprod.wikidot.com/>`_.


.. toctree::
   :maxdepth: 2
   :caption: Contents:

   install
   DFT
   TDDFT
   gwa
   bse
   optimizations
   examples/examples
   references
   modules
   contact


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
