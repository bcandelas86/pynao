.. _references:

==========
References
==========

Please, cite the following papers when using PyNAO

PyNAO and method papers
=======================

The papers on Py?NAO and its method

.. [koval2018pynao] P. Koval, M. Barbry and D. Sanchez-Portal,
   PySCF-NAO: An efficient and flexible implementation of linear response
   time-dependent density functional theory with numerical atomic orbitals,
   Computer Physics Communications, 2019,
   `10.1016/j.cpc.2018.08.004 <https://www.sciencedirect.com/science/article/pii/S0010465518302996>`_

.. [koval2010-mbptlcao] P. Koval, D. Foerster, and O. Coulaud,
   A Parallel Iterative Method for Computing Molecular Absorption Spectra,
   J. Chem. Theo. Comput. 2010,
   `10.1021/ct100280x <https://pubs.acs.org/doi/abs/10.1021/ct100280x>`_

PySCF papers
============

PyNAO was originally part of the PySCF program package and currently dependent on it.
Therefore, PySCF papers must be included when using PyNAO for your work.


.. [Qiming2020-pyscf] Qiming Sun
    Xing Zhang
    Samragni Banerjee
    Peng Bao
    Marc Barbry
    Nick S. Blunt
    Nikolay A. Bogdanov
    George H. Booth
    Jia Chen
    Zhi-Hao Cui
    Janus J. Eriksen
    Yang Gao
    Sheng Guo
    Jan Hermann
    Matthew R. Hermes
    Kevin Koh
    Peter Koval
    Susi Lehtola
    Zhendong Li
    Junzi Liu
    Narbe Mardirossian
    James D. McClain
    Mario Motta
    Bastien Mussard
    Hung Q. Pham
    Artem Pulkin
    Wirawan Purwanto
    Paul J. Robinson
    Enrico Ronca
    Elvira R. Sayfutyarova
    Maximilian Scheurer
    Henry F. Schurkus
    James E. T. Smith
    Chong Sun
    Shi-Ning Sun
    Shiv Upadhyay
    Lucas K. Wagner
    Xiao Wang
    Alec White
    James Daniel Whitfield
    Mark J. Williamson
    Sebastian Wouters
    Jun Yang
    Jason M. Yu
    Tianyu Zhu
    Timothy C. Berkelbach
    Sandeep Sharma
    Alexander Yu. Sokolov
    Garnet Kin-Lic Chan
    Recent developments in the PySCF program package,
    (2020), The Journal of Chemical Physics, 153, 2,
    `doi: 10.1063/5.0006074 <https://aip.scitation.org/doi/10.1063/5.0006074>`_

.. [Qiming2018-pyscf] Q. Sun, T. C. Berkelbach,
   N. S. Blunt, G. H. Booth, S. Guo, Z. Li, J. Liu, J. McClain, E. R. Sayfutyarova,
   S. Sharma, S. Wouters, G. K.-L. Chan (2018), PySCF: the Python‐based simulations
   of chemistry framework. WIREs Comput. Mol. Sci., 8,
   `doi:10.1002/wcms.1340 <https://onlinelibrary.wiley.com/doi/abs/10.1002/wcms.1340>`_

Thesis
======

The methods for TDDFT has been extensively described in M. Barbry Ph.D thesis

.. [Barbry2018thesis] Marc Barbry
   `Plasmons in Nanoparticles: Atomistic Ab Initio Theory for Large Systems <https://cfm.ehu.es/cfm_news/phd-thesis-defense-marc-barbry/>`_,
   2018
   
Literature
==========

.. [koval2019GW] Peter Koval, Mathias Per Ljungberg, Moritz Müller, and Daniel
   Sánchez-Portal. Toward efficient gw calculations using numerical atomic orbitals:
   Benchmarking and application to molecular dynamics simulations. J. Chem. Theory
   Comput, 15(8):4564–4580, 2019
.. [Fetter1971] A.L. Fetter and J.D. Walecka. Quantum Theory of Many-Particle
   Systems. McGraw-Hill, New York, 1971.
.. [galitskii1958] F Aryasetiawan and O Gunnarsson. The GW method. Reports on
   Progress in Physics, 61(3):237–312, mar 1998.
.. [aryasetiawan1998gw] Viktor Mikhailovich Galitskii and Arkadii Beinusovich
   Migdal. Application of quantum field theory methods to the many body problem.
   Sov. Phys. JETP, 7(96), 1958.
.. [martin_GW_2016] Richard M. Martin, Lucia Reining, and David M. Ceperley.
   Interacting Electrons: Theory and Computational Approaches. Cambridge
   University Press, 2016.
.. [Mattuck1976] R.D. Mattuck. A Guide to Feynman Diagrams in the Many-body Problem.
   Dover Books on Physics Series. Dover Publications, Incorporated, 1976.
.. [hedin1965new] Lars Hedin. New method for calculating the one-particle Green’s
   function with application to the electron-gas problem. Physical Review, 139(3A):A796, 1965
.. [martin_RPAchapter12] Richard M. Martin, Lucia Reining, and David M. Ceperley.
   The RPA and the GW approximation for the self-energy, pages 245–279.
   Cambridge University Press, 2016.
.. [RPA] David Pines and David Bohm. A collective description of electron
   interactions: Ii. collective vs individual particle aspects of the interactions.
   Phys. Rev., 85:338–353, Jan 1952.
.. [VanSetten2015] Michiel J. Van Setten, et al. GW100: Benchmarking G0W0 for
   Molecular Systems. J. Chem. Theory Comput., 11(12):5665–5687, 2015.
.. [aryasetiawan2009] Ferdi Aryasetiawan and Silke Biermann. Generalized Hedin
   equations and σGσW approximation for quantum many-body systems with spin-dependent
   interactions. Journal of Physics: Condensed Matter, 21(6):064232, jan 2009
.. [aryasetiawan2008] F. Aryasetiawan and S. Biermann. Generalized hedin’s equations
   for quantum many-body systems with spin-dependent interactions. Physical
   review letters, 100:116402, Mar 2008.
.. [ahmed2014spin] Towfiq Ahmed, Robert C Albers, Alexander V Balatsky, Christoph
   Friedrich, and Jian-Xin Zhu. GW quasi-particle calculations with spin-orbit
   coupling for the light actinides. Physical Review B, 89(3):035104, 2014.
.. [Koval2014] Peter Koval, Dietrich Foerster, and Daniel Sánchez-Portal. Fully
   self-consistent GW and quasiparticle self-consistent GW for molecules.
   Phys. Rev. B, 89(15):155417, apr 2014.
.. [gui2018] Xin Gui, Christof Holzer, and Wim Klopper. Accuracy assessment of
   GW starting points for calculating molecular excitation energies using the
   Bethe–Salpeter formalism. Journal of chemical theory and computation,
   14(4):2127–2136, 2018.
.. [Blase2011] X. Blase, C. Attaccalite, and V. Olevano. First-principles GW
   calculations for fullerenes, porphyrins, phtalo-cyanine, and other molecules
   of interest for organic photovoltaic applications. Phys. Rev. B - Condens.
   Matter Mater. Phys., 83(11):1–9, 2011.
.. [Godby1988] R. W. Godby, M. Schlüter, and L. J. Sham. Self-energy operators
   and exchange-correlation potentials in semiconductors. Phys. Rev. B,
   37(17):10159–10175, jun 1988.
.. [complexcontour] Donald J Newman and Joseph Bak. Complex analysis. Springer,
   2010.
.. [Lebegue2003] S. Lebgue, B. Arnaud, M. Alouani, and P. E. Bloechl.
   Implementation of an all-electron GW approximation based on the projector
   augmented wave method without plasmon pole approximation: Application to si,
   SiC, AlAs, InAs, NaH, and KH. Phys. Rev. B, 67(15):155208, April 2003.
.. [talman2009numsbt] JD Talman. NumSBT: A subroutine for calculating spherical
   Bessel transforms numerically. Computer Physics Communications, 180(2):332–338,
   2009.
.. [Freysoldt2008] Christoph Freysoldt, Philipp Eggert, Patrick Rinke, Arno Schindlmayr,
   and Matthias Scheffler. Screening in two dimensions: gw calculations for surfaces
   and thin films using the repeated-slab approach. Phys. Rev. B, 77:235428, Jun 2008.
.. [Ozaki2004] T. Ozaki and H. Kino. Numerical atomic basis orbitals from H to
   Kr. Phys. Rev. B, 69(19):195113, may 2004.
.. [nao2001] Javier Junquera, Óscar Paz, Daniel Sánchez-Portal, and Emilio Artacho.
   Numerical atomic orbitals for linear-scaling calculations. Phys. Rev. B,
   64(23):235111, nov 2001.
.. [Foerster2011] Dietrich Foerster, Peter Koval, and Daniel Sánchez-Portal.
   An O( N3 ) implementation of Hedin’s GW approximation for molecules. J. Chem.
   Phys., 135(7):074105, aug 2011.
.. [foerster2008] Dietrich Foerster. Elimination, in electronic structure
   calculations, of redundant orbital products. The Journal of chemical physics,
   128(3):034108, 2008. 
.. [talman1984numerical] James D Talman. Numerical calculation of four-center
   coulomb integrals. The Journal of chemical physics, 80(5):2000–2008, 1984.
.. [siesta2002] Jose M Soler, Emilio Artacho, Julian D Gale, Alberto García,
   Javier Junquera, Pablo Ordejon and Daniel Sanchez-Portal. The SIESTA method
   for ab initio order-N materials simulation, Journal of Physics: Condensed Matter,
   14 (11), 2002.
.. [siesta2020] Alberto Garcia, Nick Papior, Arsalan Akhtar, Emilio Artacho,
   Volker Blum, Emanuele Bosoni, Pedro Brandimarte, Mads Brandbyge, J. I. Cerda,
   Fabiano Corsetti, Ramon Cuadrado, Vladimir Dikan, Jaime Ferrer, Julian Gale,
   Pablo Garcia-Fernandez, V. M. Garcia-Suarez, Sandra Garcia, Georg Huhs,
   Sergio Illera, Richard Korytar, Peter Koval, Irina Lebedeva, Lin Lin,
   Pablo Lopez-Tarifa, Sara G. Mayo, Stephan Mohr, Pablo Ordejon, Andrei Postnikov,
   Yann Pouillon, Miguel Pruneda, Roberto Robles, Daniel Sanchez-Portal,
   Jose M. Soler, Rafi Ullah, Victor Wen-zhe Yu, and Javier Junquera.
   Siesta: Recent developments and applications, J. Chem. Phys. 152, 204108, 2020.
.. [Barbry2015] M. Barbry, P. Koval, F. Marchesin, R. Esteban, A. G. Borisov,
   J. Aizpurua, and D. Sanchez-Portal. Atomistic Near-Field Nanoplasmonics:
   Reaching Atomic-Scale Resolution in Nanooptics. Nano Lett. 2015, 15, 5,
   3410–3419
