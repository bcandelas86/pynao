from __future__ import print_function, division
import os
import unittest, numpy as np
from pynao import mf

dname = os.path.dirname(os.path.abspath(__file__))

class KnowValues(unittest.TestCase):

    def test_fireball(self):
        """
        Test computation of matrix elements of overlap after fireball
        """
        sv = mf(fireball="fireball.out", cd="{}/fireball_test".format(dname),
                gen_pb=False)
        s_ref = sv.hsx.s4_csr.toarray()
        s = sv.overlap_coo().toarray()

if __name__ == "__main__":
    unittest.main()

