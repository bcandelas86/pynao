from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import tddft_iter
from pyscf import gto, scf, tddft
from pyscf.data.nist import HARTREE2EV
from pynao.m_pack2den import pack2den_u
import scipy.sparse as sparse

dname = os.path.dirname(os.path.abspath(__file__))
mol = gto.M(verbose=1, atom='B 0 0 0; H 0 0.489 1.074; H 0 0.489 -1.074',
            basis='cc-pvdz', spin=3)
gto_mf = scf.UHF(mol)
gto_mf.kernel()

class KnowValues(unittest.TestCase):

    def test_142_h2b_uhf_rpa_pb(self):
        """
        This tddft_iter and compare kernel pack and sparse format for RPA
        """
        nao_mf_pack = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7,
                                 xc_code='RPA', kernel_format="pack")

        # Test noin polarizability
        comega = np.arange(0.0, 2.0, 0.01) + 1j*0.03
        pnonin = -nao_mf_pack.polariz_inter_ave(comega).imag
        data = np.array([comega.real*HARTREE2EV, pnonin])
        #np.savetxt('test_142_h2b_uhf_rpa_pb.txt', data.T, fmt=['%f','%f'])

        data_ref = np.loadtxt(dname + '/test_142_h2b_uhf_rpa_pb.txt-ref').T
        self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3))
        
        # test kernel format
        nao_mf_sparse = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7,
                                   xc_code='RPA', kernel_format="sparse",
                                   kernel_threshold=1.0e-50)

        for s in range(nao_mf_pack.nspin):
            for t in range(nao_mf_pack.nspin):
                pack_dense = pack2den_u(nao_mf_pack.ss2kernel[s][t])

                I = np.eye(nao_mf_sparse.ss2kernel[s][t].shape[0])
                diag = nao_mf_sparse.ss2kernel[s][t].diagonal()
                for i in range(I.shape[0]):
                    I[i, i] = diag[i]

                dense_from_sparse = nao_mf_sparse.ss2kernel[s][t].todense() + \
                                    nao_mf_sparse.ss2kernel[s][t].T.todense() - I

                assert np.sum(abs(pack_dense - dense_from_sparse)) < 1.0e-12

    def test_142_h2b_uhf_lda_pb(self):
        """
        This tddft_iter and compare kernel pack and sparse format for LDA
        """
        nao_mf_pack = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7,
                                 xc_code='LDA', kernel_format="pack")

        # Test inter polarizability
        comega = np.arange(0.0, 2.0, 0.01) + 1j*0.03
        pinter = -nao_mf_pack.polariz_inter_ave(comega).imag
        data = np.array([comega.real*HARTREE2EV, pinter])
        #np.savetxt('test_142_h2b_uhf_lda_pb.txt', data.T, fmt=['%f','%f'])
        data_ref = np.loadtxt(dname + '/test_142_h2b_uhf_lda_pb.txt-ref').T
        self.assertTrue(np.allclose(data_ref, data, atol=1e-6, rtol=1e-3))
        
        # test kernel format
        nao_mf_sparse = tddft_iter(gto=mol, mf=gto_mf, tol_loc=1e-5, tol_biloc=1e-7,
                                   xc_code='LDA', kernel_format="sparse",
                                   kernel_threshold=1.0e-50, verbosity=0)

        for s in range(nao_mf_pack.nspin):
            for t in range(nao_mf_pack.nspin):
                pack_dense = pack2den_u(nao_mf_pack.ss2kernel[s][t])

                I = np.eye(nao_mf_sparse.ss2kernel[s][t].shape[0])
                diag = nao_mf_sparse.ss2kernel[s][t].diagonal()
                for i in range(I.shape[0]):
                    I[i, i] = diag[i]

                dense_from_sparse = nao_mf_sparse.ss2kernel[s][t].todense() + \
                                    nao_mf_sparse.ss2kernel[s][t].T.todense() - I
                assert np.sum(abs(pack_dense - dense_from_sparse)) < 1.0e-12

if __name__ == "__main__":
    unittest.main()
