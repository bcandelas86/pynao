from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import mf

class KnowValues(unittest.TestCase):

    def test_dens_elec(self):
        """
        Compute density in coordinate space with scf, integrate and compare with
        number of electrons
        """
        from timeit import default_timer as timer
        
        sv = mf(label='water', cd=os.path.dirname(os.path.abspath(__file__)))
        dm = sv.make_rdm1()
        grid = sv.build_3dgrid_pp(level=5)
        dens = sv.dens_elec(grid.coords, dm)
        nelec = np.einsum("is,i", dens, grid.weights)[0]
        self.assertAlmostEqual(nelec, sv.hsx.nelec, 2)

if __name__ == "__main__":
    unittest.main()
