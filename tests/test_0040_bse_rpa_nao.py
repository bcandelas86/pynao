from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import bse_iter

class KnowValues(unittest.TestCase):

    def test_0040_bse_rpa_nao(self):
        """
        Compute polarization with RPA via 2-point non-local potentials (BSE solver)
        """

        dname = os.path.dirname(os.path.abspath(__file__))
        bse = bse_iter(label='water', cd=dname, xc_code='RPA', verbosity=0)
        omegas = np.linspace(0.0,2.0,150)+1j*0.01

        pxx = np.zeros(len(omegas))
        for iw,omega in enumerate(omegas):
            for ixyz in range(1):
                dip = bse.dip_ab[ixyz]
                vab = bse.apply_l(dip, omega)
                pxx[iw] = pxx[iw] - (vab.imag*dip.reshape(-1)).sum()
            
        data = np.array([omegas.real*27.2114, pxx])
        np.savetxt('water.bse_iter_rpa.omega.inter.pxx.txt', data.T, fmt=['%f','%f'])
        data_ref = np.loadtxt(dname+'/water.bse_iter_rpa.omega.inter.pxx.txt-ref')
        self.assertTrue(np.allclose(data_ref,data.T, rtol=1.0, atol=1e-05))

if __name__ == "__main__":
    unittest.main()
