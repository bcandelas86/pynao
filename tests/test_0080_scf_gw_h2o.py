from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import gw as gw_c

class KnowValues(unittest.TestCase):

    def test_scf_gw_perform_h2o(self):
        """
        reSCF then G0W0
        """
        
        dname = os.path.dirname(os.path.abspath(__file__))
        gw = gw_c(label='water', cd=dname, verbosity=0, nocc_conv=4,
                  nvrt_conv=4, perform_scf=True, tol_ia=1e-6)
        gw.kernel_gw()

        #np.savetxt('eigvals_g0w0_water_0080.txt-ref', gw.mo_energy_gw[0].T)
        ref = np.loadtxt("eigvals_g0w0_water_0080.txt-ref")
        for e, eref in zip(gw.mo_energy_gw[0,0,:], ref):
            self.assertAlmostEqual(e, eref)

if __name__ == "__main__":
    unittest.main()
