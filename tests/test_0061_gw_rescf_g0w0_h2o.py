from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import gw as gw_c

class KnowValues(unittest.TestCase):

    def test_rescf(self):
        """
        reSCF then G0W0
        """

        dname = os.path.dirname(os.path.abspath(__file__))
        gw = gw_c(label='water', cd=dname, verbosity=0, nocc=8, nvrt=6,
                  nocc_conv=4, nvrt_conv=4, rescf=True, tol_ia=1e-6,)
        gw.kernel_gw()
        
        #np.savetxt('eigvals_g0w0_pyscf_rescf_water_0061.txt-ref', gw.mo_energy_gw[0,:,:].T)
        ref = np.loadtxt("eigvals_g0w0_pyscf_rescf_water_0061.txt-ref")
        for e, eref in zip(gw.mo_energy_gw[0,0,:], ref):
            self.assertAlmostEqual(e, eref)

if __name__ == "__main__":
    unittest.main()
