from __future__ import print_function, division
import unittest, numpy as np

from pynao.log_mesh import funct_log_mesh
from pynao.m_log_interp import log_interp_c

class KnowValues(unittest.TestCase):

    def test_log_interp_sca(self):
        """
        Test the interpolation facility from the class log_interp_c
        """
        rr, pp = funct_log_mesh(1024, 0.01, 20.0)
        log_interp = log_interp_c(rr)
        gc = 1.2030
        ff = np.array([np.exp(-gc*r**2) for r in rr])
        for r in np.linspace(0.009, 25.0, 100):
            y = log_interp(ff, r)
            yrefa = np.exp(-gc*r**2)
            self.assertAlmostEqual(y, yrefa)

    def test_log_interp_vec(self):
        """
        Test the interpolation facility for an array arguments from the
        class log_interp_c
        """
        rr, pp = funct_log_mesh(1024, 0.01, 20.0)
        log_interp = log_interp_c(rr)
        gcs = np.array([1.2030, 3.2030, 0.7, 10.0])
        ff = np.array([[np.exp(-gc*r**2) for r in rr] for gc in gcs])
        for r in np.linspace(0.009, 25.0, 100):
            yyref = np.exp(-gcs*r**2)
            yy = log_interp(ff, r)
            for y, yref in zip(yy, yyref):
                self.assertAlmostEqual(y,yref)

    def test_log_interp_diff(self):
        """
        Test the differentiation facility from the class log_interp_c
        """
        rr, pp = funct_log_mesh(1024, 0.001, 20.0)
        logi = log_interp_c(rr)
        gc = 1.2030
        ff = np.array([np.exp(-gc*r**2) for r in rr])
        ffd_ref = np.array([np.exp(-gc*r**2)*(-2.0*gc*r) for r in rr])
        ffd = logi.diff(ff)
        ffd_np = np.gradient(ff, rr)
        s = 3
        for r, d, dref, dnp in zip(rr[s:], ffd[s:], ffd_ref[s:], ffd_np[s:]):
            self.assertAlmostEqual(d,dref)
      
if __name__ == "__main__":
    unittest.main()
