from __future__ import print_function, division
import unittest, numpy as np
from pyscf import gto, scf
from pynao import gw as gw_c

mol = gto.M(verbose=1, atom='''Mn 0 0 0;''', basis='cc-pvdz', spin=5)
gto_mf_uhf = scf.UHF(mol)
gto_mf_uhf.kernel()
dm = gto_mf_uhf.make_rdm1()
jg,kg = gto_mf_uhf.get_jk()

class KnowValues(unittest.TestCase):

    def test_mn_gw_0086(self):
        from pynao.m_fermi_dirac import fermi_dirac_occupations
        """
        Spin-resolved case GW procedure
        """
        gw = gw_c(mf=gto_mf_uhf, gto=mol, verbosity=0)
        self.assertEqual(gw.nspin, 2)

if __name__ == "__main__":
    unittest.main()
