from __future__ import print_function, division
import os,unittest,numpy as np
from pynao import tddft_iter

class KnowValues(unittest.TestCase):

    def test_fxc(self):
        """
        Compute TDDFT interaction kernel
        """

        td = tddft_iter(label='water', cd=os.path.dirname(os.path.abspath(__file__)),
                        xc_code='LDA,PZ', kernel_format="sparse",
                        kernel_threshold=1.0e-12)

if __name__ == "__main__":
    unittest.main()
