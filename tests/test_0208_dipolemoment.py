from __future__ import print_function, division
import os, unittest, numpy as np
import pynao.m_comp_dipole_moment as cdm

def lorentzian(w, w0, gam):
    return (gam + 1j*w)/(gam**2 + 2j*gam*w - w**2 + w0**2)

def florentzian(t, w0, gam):
    f0=np.cos(w0*t)*np.exp(-gam*t)
    f0[t<0]=0
    return f0

class KnowValues(unittest.TestCase):

    def test_dipole_moment(self):
        """
        Test the computation of dipole moment based on Lorentzian polarizability.
        """
        N = 1001
        x = np.linspace(0, 15, N)
        x0 = 3
        gam = 0.2
        y = lorentzian(x,x0,gam)

        dm = cdm.comp_dipole_moment_kick(x,y)
        t = cdm.comp_time(x)

        fa = florentzian(t,x0,gam)
        m1 = np.mean(np.abs(fa-dm))

        self.assertTrue(np.allclose(m1, 0, atol=5e-3))

if __name__ == "__main__":
    unittest.main()
