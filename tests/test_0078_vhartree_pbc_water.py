from __future__ import print_function, division
import unittest
import os
import numpy as np
from timeit import default_timer as timer

from pynao import nao as nao_c
from pynao import mf as mf_c
from pynao.m_siesta_units import siesta_conv_coefficients

class KnowValues(unittest.TestCase):

    def test_0078_build_3dgrid_pp_pbc_water(self):
        """
        Test Hartree potential on equidistant grid with Periodic Boundary
        Conditions
        """
        dname = os.path.dirname(os.path.abspath(__file__))
        nao = nao_c(label='water', cd=dname, Ecut=100.0)
        gg,dv = nao.mesh3d.rr, nao.mesh3d.dv
        self.assertAlmostEqual(dv, 0.007621441417508375)
        self.assertEqual(len(gg), 3)
        self.assertTrue(gg[0].shape==(72,3))
        self.assertTrue(gg[1].shape==(54,3))
        self.assertTrue(gg[2].shape==(54,3))
        self.assertEqual(nao.mesh3d.size, 72*54*54)

    def test_0078_vhartree_pbc_water(self):
        """
        Test Hartree potential on equidistant grid with Periodic Boundary
        Conditions
        """
        import os
        dname = os.path.dirname(os.path.abspath(__file__))
        mf = mf_c(label='water', cd=dname, gen_pb=False, Ecut=100.0)
        d = abs(np.dot(mf.ucell_mom(), mf.ucell)-(2*np.pi)*np.eye(3)).sum()
        self.assertTrue(d<1e-15)
        g = mf.mesh3d.get_3dgrid()
        dens = mf.dens_elec(g.coords, mf.make_rdm1()).reshape(mf.mesh3d.shape)
        ts = timer()
        vh = mf.vhartree_pbc(dens)
        tf = timer()
        E_Hartree = 0.5*(vh*dens*g.weights).sum()*siesta_conv_coefficients["ha2ev"]
        # previous value for E_Hartree: 382.8718239023864
        # changed the 2020-08-11 for merge request 41
        # https://gitlab.com/mbarbry/pynao/-/merge_requests/41
        # siesta:       Hartree =     382.890331
        self.assertAlmostEqual(E_Hartree, 382.8718205188019)

if __name__ == "__main__":
    unittest.main()
