from __future__ import print_function, division
import unittest

class KnowValues(unittest.TestCase):

    def test_0006_02_nao_noghost(self):
        import os
        from pynao import nao
        from pynao.m_overlap_am import overlap_am

        dname = os.path.dirname(os.path.abspath(__file__)) + '/test_0006_ag13_noghost'
        sv = nao(label='siesta', cd=dname)
        aa = sv.vna_coo().toarray()
        oc = sv.overlap_check(funct=overlap_am)
        self.assertTrue(oc[0])
    

if __name__ == "__main__": 
    unittest.main()
