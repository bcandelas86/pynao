from __future__ import print_function, division
import unittest

class KnowValues(unittest.TestCase):

    def test_0006_01_nao_ghost(self):
        import os
        from pynao import nao
        from pynao.m_overlap_am import overlap_am
        
        dname = os.path.dirname(os.path.abspath(__file__)) + '/test_0006_ag13_ghost'
        sv = nao(label='siesta', cd=dname)
        oc = sv.overlap_check(funct=overlap_am)
        self.assertTrue(oc[0])
        vna = sv.vna_coo().toarray()
        
    

if __name__ == "__main__": 
    unittest.main()
