FROM ubuntu:20.04

# Install anaconda
# From https://github.com/ContinuumIO/docker-images/blob/master/anaconda3/debian/Dockerfile
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8
ENV TZ="Europe/Paris"
RUN debconf-set-selections

RUN apt-get update --fix-missing && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y htop tmux vim wget bzip2 ca-certificates libglib2.0-0 libxext6 libsm6 libxrender1 git mercurial subversion && \
    apt-get install -y python3 python3-dev python3-pip && \
    apt-get clean

RUN mkdir -p /opt/dev
# It is not working to set the environment here. It should be done when
# running the docker image
#ENV MKL_NUM_THREADS="$(nproc --all)"
#ENV NUMTHREADS="$(nproc --all)"
#ENV OMP_NUM_THREADS="$(nproc --all)"
ENV OMP_PROC_BIND=FALSE

WORKDIR /usr/src/app

RUN apt-get update && apt-get -y install git gcc gfortran build-essential libopenblas-dev libfftw3-dev make cmake zlib1g-dev python3-sphinx nvidia-driver-460 nvidia-cuda-toolkit
RUN pip3 install numpy scipy==1.4.1 matplotlib pyaml numba h5py ase pytest pytest-cov wheel sphinx_rtd_theme jupyterlab cupy
RUN pip3 install pandas

# Clone siesta and pynao
RUN cd /opt && git clone https://gitlab.com/siesta-project/siesta.git
RUN cd /opt && git clone https://gitlab.com/mbarbry/pynao.git && cd /opt/pynao && \
    git fetch && git checkout master && git pull

# Compile siesta
RUN cd /opt/siesta/Obj && sh ../Src/obj_setup.sh
RUN cp /opt/pynao/share/siesta-arch.make/arch.make.openmp.openblas /opt/siesta/Obj/arch.make
RUN cd /opt/siesta/Obj && make

# install PySCF
RUN cd /opt && git clone https://github.com/pyscf/pyscf.git
RUN cd /opt/pyscf && git fetch && git checkout master && git pull
RUN cd /opt/pyscf/pyscf/lib && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=Release .. && make

ENV PYTHONPATH=/opt/pyscf:$PYTHONPATH
ENV LD_LIBRARY_PATH=/opt/pyscf/pyscf/lib/deps/lib:${LD_LIBRARY_PATH}

# install PyNAO
RUN cd /opt/pynao && \
    cp lib/cmake_user_inc_examples/cmake.user.inc-gnu-openmp-openblas lib/cmake.arch.inc && \
    python3 setup.py build
RUN cd /opt/pynao/share/pseudo && tar -xzf pseudos.tar.gz

ENV PATH="/opt/siesta/Obj:${PATH}"
ENV ASE_SIESTA_COMMAND="siesta < PREFIX.fdf > PREFIX.out"
ENV PYTHONPATH="/opt/pynao:${PYTHONPATH}"
ENV SIESTA_PP_PATH=/opt/pynao/share/pseudo

RUN cp -r /opt/pynao/examples/tutorials /usr/src/app/tutorials-pynao

#CMD [ "/bin/bash" ]
# Declare port used by jupyter-lab
EXPOSE 8888

CMD ["jupyter", "lab", "--port=8888", "--no-browser", "--ip=0.0.0.0", "--allow-root"]
