from __future__ import print_function, division
import numpy as np
from timeit import default_timer as timer
import scipy.sparse as sparse

def vxc_csr_spin1(self, threshold=1.0e-14, **kw):
    """
    Computes the exchange-correlation matrix elements in sparse format

    Input parameters:
    self: tddft_iter class (pynao.tddft_iter)
        this must have arrays of coordinates and species, etc
    threshold: float
        the threshold for determining if a matrix element should be added to the matrix

    Output parameters:
    
    kernel: csr sparse matrix (scipy.sparse.csr_matrix)
        the Vxc kernel in csr sparse format
    """
    from pynao.m_xc_scalar_ni import xc_scalar_ni
    from pynao.m_ao_matelem import ao_matelem_c
    from pynao.m_comp_coulomb_sparse import fill_coo_matrix_tiangular

    dm = kw['dm'] if 'dm' in kw else self.make_rdm1()
    kernel = kw['kernel'] if 'kernel' in kw else None
    ao_log = kw['ao_log'] if 'ao_log' in kw else self.ao_log
    xc_code = kw['xc_code'] if 'xc_code' in kw else self.xc_code
    kw.pop('xc_code',None)
    dtype = kw['dtype'] if 'dtype' in kw else self.dtype

    aome = ao_matelem_c(self.ao_log.rr, self.ao_log.pp, self, dm)
    me = aome.init_one_set(self.ao_log) if ao_log is None else aome.init_one_set(ao_log)
    atom2s = np.zeros((self.natm+1), dtype=np.int32)
    for atom, sp in enumerate(self.atom2sp):
        atom2s[atom+1] = atom2s[atom] + me.ao1.sp2norbs[sp]
    sp2rcut = np.array([max(mu2rcut) for mu2rcut in me.ao1.sp_mu2rcut])
    norbs = atom2s[-1]

    if kernel is None:
        kernel = sparse.csr_matrix((norbs, norbs), dtype=dtype)
    else:
        assert sparse.isspmatrix_csr(kernel)

    ttotal = 0.0
    for atom1, [sp1, rv1, s1, f1] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                   atom2s, atom2s[1:])):
        t1 = timer()
        tmpmat = sparse.csr_matrix((norbs, norbs), dtype=dtype)
        for atom2, [sp2, rv2, s2, f2] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                       atom2s,atom2s[1:])):

            if atom2 > atom1:
                continue

            if (sp2rcut[sp1] + sp2rcut[sp2])**2 <= sum((rv1 - rv2)**2):
                continue
            
            iab2block = xc_scalar_ni(me, sp1, rv1, sp2, rv2, xc_code=xc_code, **kw)
            assert len(iab2block) == 1
            rows, cols, data = fill_coo_matrix_tiangular(s1, s2, iab2block[0],
                                                         threshold=threshold)
            tmpmat += sparse.coo_matrix((data, (rows, cols)), dtype=dtype,
                                        shape=(norbs, norbs)).tocsr()

        kernel += tmpmat

        t2 = timer()
        ttotal += t2 - t1
        if self.verbosity > 2:
            print("Vxc kernel: atm {}/{}: {} ".format(atom1, self.natm, t2-t1))

    norbs = atom2s[-1]
    if self.verbosity > 2:
        print("{}\t====> Vxc kernel timing: {}".format(__name__, ttotal))

    return kernel

def vxc_csr_spin2(self, threshold, **kw):
    """
    Computes the exchange-correlation matrix elements in sparse format

    Input parameters:
    self: tddft_iter class (pynao.tddft_iter)
        this must have arrays of coordinates and species, etc
    threshold: float
        the threshold for determining if a matrix element should be added to the matrix

    Output parameters:
    
    kernel: list of csr sparse matrix (scipy.sparse.csr_matrix)
        the Vxc kernel in csr sparse format
    """
    from pynao.m_xc_scalar_ni import xc_scalar_ni
    from pynao.m_ao_matelem import ao_matelem_c
    from pynao.m_comp_coulomb_sparse import fill_coo_matrix_tiangular

    dm = kw['dm'] if 'dm' in kw else self.make_rdm1()
    kernel = kw['kernel'] if 'kernel' in kw else None
    ao_log = kw['ao_log'] if 'ao_log' in kw else self.ao_log
    xc_code = kw['xc_code'] if 'xc_code' in kw else self.xc_code
    kw.pop('xc_code',None)
    dtype = kw['dtype'] if 'dtype' in kw else self.dtype

    aome = ao_matelem_c(self.ao_log.rr, self.ao_log.pp, self, dm)
    me = aome.init_one_set(self.ao_log) if ao_log is None else aome.init_one_set(ao_log)
    atom2s = np.zeros((self.natm+1), dtype=np.int32)
    for atom, sp in enumerate(self.atom2sp):
        atom2s[atom+1] = atom2s[atom] + me.ao1.sp2norbs[sp]
    sp2rcut = np.array([max(mu2rcut) for mu2rcut in me.ao1.sp_mu2rcut])
    norbs = atom2s[-1]

    if kernel is None:
        spmat = [sparse.csr_matrix((norbs, norbs), dtype=dtype) for i in range((self.nspin-1)*2+1)]
    else:
        assert sparse.isspmatrix_csr(kernel)
        spmat = [kernel for i in range((self.nspin-1)*2+1)]

    ttotal = 0.0
    for atom1, [sp1, rv1, s1, f1] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                   atom2s, atom2s[1:])):
        t1 = timer()
        tmpmat = [sparse.csr_matrix((norbs, norbs), dtype=dtype) for i in range((self.nspin-1)*2+1)]
        for atom2, [sp2, rv2, s2, f2] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                       atom2s,atom2s[1:])):

            if (sp2rcut[sp1] + sp2rcut[sp2])**2 <= sum((rv1 - rv2)**2):
                continue

            
            iab2block = xc_scalar_ni(me, sp1, rv1, sp2, rv2, xc_code=xc_code, **kw)
            for i, b in enumerate(iab2block):
                rows, cols, data = fill_coo_matrix_tiangular(s1, s2, b, threshold=threshold)
                tmpmat[i] += sparse.coo_matrix((data, (rows, cols)), dtype=dtype,
                                               shape=(norbs, norbs)).tocsr()

        for i, mat in enumerate(tmpmat):
            spmat[i] += mat

        t2 = timer()
        ttotal += t2 - t1
        if self.verbosity > 2:
            print("Vxc kernel: atm {}/{}: {} ".format(atom1, self.natm, t2-t1))

    norbs = atom2s[-1]
    if self.verbosity > 2:
        print("{}\t====> Vxc kernel timing: {}".format(__name__, ttotal))

    #import sys
    #sys.exit()
    return spmat

def vxc_lil(self, **kw):
    """
    Computes the exchange-correlation matrix elements

    Input parameters:
    sv : (System Variables)
         this must have arrays of coordinates and species, etc

    Output parameters:
        lil: (list of lil matrix)
    """
    from pynao.m_xc_scalar_ni import xc_scalar_ni
    from pynao.m_ao_matelem import ao_matelem_c

    dm = kw['dm'] if 'dm' in kw else self.make_rdm1()
    kernel = kw['kernel'] if 'kernel' in kw else None
    ao_log = kw['ao_log'] if 'ao_log' in kw else self.ao_log
    xc_code = kw['xc_code'] if 'xc_code' in kw else self.xc_code
    kw.pop('xc_code',None)
    dtype = kw['dtype'] if 'dtype' in kw else self.dtype

    aome = ao_matelem_c(self.ao_log.rr, self.ao_log.pp, self, dm)
    me = aome.init_one_set(self.ao_log) if ao_log is None else aome.init_one_set(ao_log)
    atom2s = np.zeros((self.natm+1), dtype=np.int32)
    for atom, sp in enumerate(self.atom2sp):
        atom2s[atom+1] = atom2s[atom] + me.ao1.sp2norbs[sp]
    sp2rcut = np.array([max(mu2rcut) for mu2rcut in me.ao1.sp_mu2rcut])

    lil = [sparse.lil_matrix((atom2s[-1],atom2s[-1]), dtype=dtype) for i in range((self.nspin-1)*2+1)]

    print("get vxc_lil")
    for atom1, [sp1, rv1, s1, f1] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                   atom2s, atom2s[1:])):
        t1 = timer()
        for atom2, [sp2, rv2, s2, f2] in enumerate(zip(self.atom2sp, self.atom2coord,
                                                       atom2s,atom2s[1:])):

            if (sp2rcut[sp1] + sp2rcut[sp2])**2 <= sum((rv1 - rv2)**2):
                continue

            iab2block = xc_scalar_ni(me, sp1, rv1, sp2, rv2, xc_code=xc_code, **kw)
            for i, b in enumerate(iab2block):
                lil[i][s1:f1,s2:f2] = b[:, :]
        t2 = timer()
        print("atm {}/{}: {} ".format(atom1, self.natm, t2-t1))

    return lil
