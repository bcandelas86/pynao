from __future__ import print_function, division
import numpy as np
from pynao.m_siesta_units import siesta_conv_coefficients

def fireball_get_eigen_dat(cd):
    """
    Calls
    """
    with open(cd+'/eigen.dat', "r") as fl:
        s = fl.readlines()
    nn = [nspin,norbs] = list(map(int, s[0].split()))
    if nspin>1:
        raise RuntimeError("nspin>1 ==> don't know how to read this...")
    i2e = []
    for line in s[2:]:
        i2e += list(map(float, line.split()))
    return np.array(i2e)/siesta_conv_coefficients["ha2ev"]
