from __future__ import division
import numba as nb
import numpy as np

@nb.jit(nopython=True, parallel=False)
def div_eigenenergy_numba(n2e, n2f, nfermi, vstart, comega, nm2v_re, nm2v_im):
    """
    multiply the temporary matrix by (fn - fm) (frac{1.0}{w - (Em-En) -1} -
        frac{1.0}{w + (Em - En)})
    using numba
    """

    neigv = n2e.shape[-1]
    a0 = comega.real**2 - comega.imag**2
    b = 2*comega.real*comega.imag
    
    for n in range(nfermi):
        en = n2e[n]
        fn = n2f[n]

        for m in range(neigv-vstart):
            em = n2e[m+vstart]
            fm = n2f[m+vstart]

            a = a0 - (em -en)**2
            factor = 2*(fn - fm)*(em - en)
            den = a**2 + b**2

            nm2v_re_nm = factor*(a*nm2v_re[n, m] + b*nm2v_im[n, m])/den
            nm2v_im_nm = factor*(a*nm2v_im[n, m] - b*nm2v_re[n, m])/den
            #nm2v = nm2v_re[n, m] + 1.0j*nm2v_im[n, m]
            #nm2v = nm2v * (fn-fm) * \
            #  ( 1.0 / (comega - (em - en)) - 1.0 / (comega + (em - en)) )
            #
            nm2v_re[n, m] = nm2v_re_nm
            nm2v_im[n, m] = nm2v_im_nm

    for n in range(vstart+1, nfermi):
        nm2v_re[n, 0:n-vstart] = 0.0
        nm2v_im[n, 0:n-vstart] = 0.0

@nb.jit(nopython=True)
def mat_mul_numba(a, b):
    return a*b
